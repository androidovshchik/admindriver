package ru.mrcpp.appdrive.injection.component;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;

import ru.mrcpp.appdrive.injection.ApplicationContext;
import ru.mrcpp.appdrive.injection.module.ApplicationModule;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    @ApplicationContext
    Context context();

    Application application();
}
