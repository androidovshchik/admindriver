package ru.mrcpp.appdrive.injection.module;

import android.app.Application;
import android.content.Context;

import dagger.Module;
import dagger.Provides;

import ru.mrcpp.appdrive.injection.ApplicationContext;

@Module
public class ApplicationModule {

    protected final Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    Application provideApplication() {
        return application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return application;
    }
}
