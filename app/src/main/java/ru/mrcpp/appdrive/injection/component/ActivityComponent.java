package ru.mrcpp.appdrive.injection.component;

import dagger.Component;

import ru.mrcpp.appdrive.injection.PerActivity;
import ru.mrcpp.appdrive.injection.module.ActivityModule;
import ru.mrcpp.appdrive.ui.login.ActivityLogin;
import ru.mrcpp.appdrive.ui.main.ActivityMainAdmin;
import ru.mrcpp.appdrive.ui.main.ActivityMainDriver;
import ru.mrcpp.appdrive.ui.registration.ActivityRegistration;
import ru.mrcpp.appdrive.ui.splash.ActivitySplash;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void injectSplash(ActivitySplash activity);

    void injectMainDriver(ActivityMainDriver activity);

    void injectMainAdmin(ActivityMainAdmin activity);

    void injectLogin(ActivityLogin activity);

    void injectRegistration(ActivityRegistration activity);
}
