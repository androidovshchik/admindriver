package ru.mrcpp.appdrive.ui.main;

import android.Manifest;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.SwitchCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;

import org.json.JSONObject;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;

import ru.mrcpp.appdrive.Constants;
import ru.mrcpp.appdrive.R;
import ru.mrcpp.appdrive.data.GeoService;
import ru.mrcpp.appdrive.data.remote.RequestGetStatus;
import ru.mrcpp.appdrive.data.remote.RequestListener;
import ru.mrcpp.appdrive.receivers.MessageReceiver;
import ru.mrcpp.appdrive.ui.base.ActivityBase;

import timber.log.Timber;

public class ActivityMainDriver extends ActivityBase implements RequestListener {

    private static ActivityMainDriver instance;

    public static ActivityMainDriver getInstance(){
        return instance;
    }

    private static final int REQUEST_FINE_LOCATION = 0;

    private MessageReceiver receiver = new MessageReceiver();

    private ScheduledFuture<?> checker;

    private RequestGetStatus request;

    private int status = Constants.STATUS_ONLINE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_driver);
        getActivityComponent().injectMainDriver(this);
        ButterKnife.bind(this);
        instance = this;

        showStatus(R.string.driver_status_not_in_line);

        request = new RequestGetStatus(this);

        ScheduledExecutorService timer = Executors.newScheduledThreadPool(1);
        checker = timer.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                if (status == Constants.STATUS_PENDING) {
                    Timber.i("Checking driver status...");
                    request.enqueue("88e1a82fhh5iii680bm7vafge0");
                }
            }
        }, 0, 5, TimeUnit.SECONDS);

        if (hasPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            startService(GeoService.getStartIntent(this));
        } else {
            ActivityCompat.requestPermissions(this, new String[] {
                            Manifest.permission.ACCESS_FINE_LOCATION
                    }, REQUEST_FINE_LOCATION);
        }
    }

    @Override
    public void onSuccess(JSONObject response) {

    }

    @Override
    public void onError(final String message, @StringRes final int resString) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showMessage(message, resString);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_driver_main, menu);
        final MenuItem controller = menu.findItem(R.id.action_queue);
        controller.setActionView(R.layout.switch_controller);
        ((SwitchCompat) controller.getActionView())
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if (checked)  {
                    status = Constants.STATUS_PENDING;
                    showStatus(R.string.driver_status_not_confirmed);
                } else {
                    status = Constants.STATUS_ONLINE;
                    showStatus(R.string.driver_status_not_in_line);
                }
            }
        });
        return true;
    }

    private void showStatus(@StringRes int id) {
        setTitle(getString(R.string.driver_status, getString(id)));
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(MessageReceiver.ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_FINE_LOCATION: {
                if (hasPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    startService(GeoService.getStartIntent(this));
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (checker != null && !checker.isCancelled()) {
            checker.cancel(false);
        }
    }
}
