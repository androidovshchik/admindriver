package ru.mrcpp.appdrive.ui.registration;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.text.InputType;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;

import com.dd.processbutton.iml.ActionProcessButton;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import ru.mrcpp.appdrive.BuildConfig;
import ru.mrcpp.appdrive.R;
import ru.mrcpp.appdrive.data.local.Prefs;
import ru.mrcpp.appdrive.data.remote.RequestConfirm;
import ru.mrcpp.appdrive.data.remote.RequestListener;
import ru.mrcpp.appdrive.data.remote.RequestRegister;
import ru.mrcpp.appdrive.ui.base.ActivityBase;
import ru.mrcpp.appdrive.ui.login.ActivityLogin;
import ru.mrcpp.appdrive.utils.DialogFactory;
import ru.mrcpp.appdrive.utils.NetworkUtil;

import timber.log.Timber;

public class ActivityRegistration extends ActivityBase {

    @BindView(R.id.imei)
    EditText imei;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.surname)
    EditText surname;
    @BindView(R.id.patronymic)
    EditText patronymic;
    @BindView(R.id.submit)
    ActionProcessButton submit;

    private RequestRegister register;
    private RequestConfirm confirm;

    private String phoneParam;
    private String imeiParam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_driver_register);
        getActivityComponent().injectRegistration(this);
        setTitle(R.string.title_driver_register);
        ButterKnife.bind(this);

        submit.setText(R.string.register);

        if (savedInstanceState == null) {
            if (BuildConfig.DEBUG) {
                imei.setText("testimei");
                phone.setText("+79174130196");
                surname.setText("Иванов");
                name.setText("Иван");
                patronymic.setText("Иванович");
            }
        }

        register = new RequestRegister(new RequestListener() {

            @Override
            public void onSuccess(final JSONObject response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        boolean errorInJson = false;
                        try {
                            Timber.i("token is %s", response.getString("token"));
                            prefs.putStr(Prefs.STR_TOKEN, response.getString("token"));
                            prefs.putLong(Prefs.LONG_TOKEN_TIME_RECEIVING, System.currentTimeMillis());
                            Timber.i("sms is %s", response.getString("sms"));
                            prefs.putStr(Prefs.STR_SMS, response.getString("sms"));
                        } catch (JSONException e) {
                            Timber.e(e.toString());
                            errorInJson = true;
                        }
                        if (!errorInJson) {
                            submit.setProgress(100);
                            showConfirmationDialog();
                        } else {
                            showMessage(R.string.error_json);
                        }
                    }
                });
            }

            @Override
            public void onError(String message, @StringRes int resString) {
                ActivityRegistration.this.onError(message, resString);
            }
        });
        confirm = new RequestConfirm(new RequestListener() {

            @Override
            public void onSuccess(final JSONObject response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        boolean errorInJson = false;
                        try {
                            Timber.i("session_id is %s", response.getString("session_id"));
                            prefs.putStr(Prefs.STR_SESSION_ID, response.getString("session_id"));
                            Timber.i("user_info is %s", response.getString("user_info"));
                            prefs.putStr(Prefs.STR_JSON_USER_INFO, response.getString("user_info"));
                        } catch (JSONException e) {
                            Timber.e(e.toString());
                            errorInJson = true;
                        }
                        if (!errorInJson) {
                            startActivity(new Intent(ActivityRegistration.this, ActivityLogin.class));
                            finish();
                        } else {
                            showMessage(R.string.error_json);
                        }
                    }
                });
            }

            @Override
            public void onError(String message, @StringRes int resString) {
                ActivityRegistration.this.onError(message, resString);
            }
        });
    }

    private void onError(final String message, final @StringRes int resString) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                submit.setProgress(-1);
                if (message == null && resString != 0) {
                    showMessage(resString);
                } else {
                    showMessage(message);
                }
            }
        });
    }

    @OnClick(R.id.submit)
    void register() {
        if (!validFields(imei, phone, name, surname, patronymic)) {
            showMessage(R.string.error_empty_fields);
            return;
        }
        long timeReceiving = prefs.getLong(Prefs.LONG_TOKEN_TIME_RECEIVING);
        if (System.currentTimeMillis() - timeReceiving < 10 * 60 * 1000) {
            showConfirmationDialog();
            return;
        } else {
            prefs.remove(Prefs.LONG_TOKEN_TIME_RECEIVING);
            prefs.remove(Prefs.STR_TOKEN);
            prefs.remove(Prefs.STR_SMS);
        }
        if(NetworkUtil.isNetworkConnected(this)) {
            submit.setProgress(25);
        } else {
            submit.setProgress(-1);
            showMessage(R.string.no_connection);
            return;
        }
        phoneParam = phone.getText().toString().trim();
        imeiParam = imei.getText().toString().trim();
        register.enqueue(phoneParam, imeiParam);
    }

    private boolean validFields(EditText... views) {
        for (EditText view : views) {
            if (view.getText().toString().trim().isEmpty()) {
                return false;
            }
        }
        return true;
    }

    private void showConfirmationDialog() {
        final AppCompatEditText code = DialogFactory.createInput(this, InputType.TYPE_CLASS_NUMBER);
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                .setTitle(R.string.confirm)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                final String userCode = code.getText().toString().trim();
                                Timber.i("Registration code input: %s", userCode);
                                if (!prefs.getStr(Prefs.STR_SMS).equals(userCode)) {
                                    showMessage(R.string.error_wrong_code);
                                } else {
                                    prefs.remove(Prefs.LONG_TOKEN_TIME_RECEIVING);
                                    if (phoneParam != null && imeiParam != null) {
                                        confirm.enqueue(phoneParam, imeiParam,
                                                prefs.getStr(Prefs.STR_TOKEN), prefs.getStr(Prefs.STR_SMS));
                                    }
                                }
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton(android.R.string.cancel, null);
        final ViewGroup layout = DialogFactory.createLayout(this, code);
        final AlertDialog alert = dialog.create();
        alert.setView(layout);
        alert.show();
    }
}
