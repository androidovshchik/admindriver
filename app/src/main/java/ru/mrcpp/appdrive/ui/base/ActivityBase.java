package ru.mrcpp.appdrive.ui.base;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import ru.mrcpp.appdrive.AppDrive;
import ru.mrcpp.appdrive.R;
import ru.mrcpp.appdrive.data.local.Prefs;
import ru.mrcpp.appdrive.injection.component.ActivityComponent;
import ru.mrcpp.appdrive.injection.component.DaggerActivityComponent;
import ru.mrcpp.appdrive.injection.module.ActivityModule;

import timber.log.Timber;

public class ActivityBase extends AppCompatActivity {

    private ActivityComponent activityComponent;

    protected Prefs prefs = Prefs.getInstance();

    @SuppressWarnings("deprecation")
    public ActivityComponent getActivityComponent() {
        if(activityComponent == null) {
            activityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(this))
                    .applicationComponent(AppDrive.getInstance().getComponent())
                    .build();
        }
        return activityComponent;
    }

    protected void showMessage(String message) {
        View root = findViewById(R.id.coordinator);
        if(root != null) {
            Snackbar snackbar = Snackbar.make(root, message, Snackbar.LENGTH_SHORT);
            snackbar.show();
        }
    }

    public void showMessage(@StringRes int resString) {
        if (resString != 0) {
            showMessage(getString(resString));
        }
    }

    protected void showMessage(String message, @StringRes int resString) {
        if (message == null && resString != 0) {
            showMessage(getString(resString));
        } else {
            showMessage(message);
        }
    }

    protected static boolean hasPermission(Context context, String permission) {
        int result = context.checkCallingOrSelfPermission(permission);
        Timber.i("Permission %s is %s", permission,
                result == PackageManager.PERMISSION_GRANTED ? "GRANTED" : "DENIED");
        return result == PackageManager.PERMISSION_GRANTED;
    }

    protected static boolean hasAllPermissions(Context context, String... permissions) {
        for(String permission : permissions) {
            if (!hasPermission(context, permission)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if(menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
