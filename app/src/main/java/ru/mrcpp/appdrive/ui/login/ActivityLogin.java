package ru.mrcpp.appdrive.ui.login;

import android.os.Bundle;
import android.view.WindowManager;
import android.widget.EditText;

import com.dd.processbutton.iml.ActionProcessButton;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import ru.mrcpp.appdrive.R;
import ru.mrcpp.appdrive.data.remote.RequestBase;
import ru.mrcpp.appdrive.data.remote.RequestRegister;
import ru.mrcpp.appdrive.ui.base.ActivityBase;
import ru.mrcpp.appdrive.utils.NetworkUtil;

public class ActivityLogin extends ActivityBase {

    @BindView(R.id.auto)
    EditText numberAuto;
    @BindView(R.id.ticket)
    EditText numberTicket;
    @BindView(R.id.call)
    EditText call;
    @BindView(R.id.submit)
    ActionProcessButton login;

    private RequestRegister request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_driver_login);
        getActivityComponent().injectLogin(this);
        setTitle(R.string.title_driver_login);
        ButterKnife.bind(this);
        login.setText(R.string.login);
        /*request = new RequestRegister(new RequestBase.RequestListener() {

            @Override
            public void onSuccess(JSONObject response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        login.setProgress(100);
                    }
                });
            }

            @Override
            public void onError(final String message) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        login.setProgress(-1);
                        showMessage(message);
                    }
                });
            }
        });*/
    }

    @OnClick(R.id.submit)
    void register() {
        if(NetworkUtil.isNetworkConnected(this)) {
            login.setProgress(33);
        } else {
            login.setProgress(-1);
            return;
        }
        if (validFields(numberAuto, numberTicket, call)) {
            showMessage(R.string.error_empty_fields);
        }
        String numberAuto = this.numberAuto.getText().toString().trim();
        //request.enqueue(phone, imei);
    }

    private boolean validFields(EditText... views) {
        for (EditText view : views) {
            if (view.getText().toString().trim().isEmpty()) {
                return false;
            }
        }
        return true;
    }
}
