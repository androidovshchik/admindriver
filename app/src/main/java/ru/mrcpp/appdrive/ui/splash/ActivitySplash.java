package ru.mrcpp.appdrive.ui.splash;

import android.content.Intent;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;

import ru.mrcpp.appdrive.R;
import ru.mrcpp.appdrive.ui.base.ActivityBase;
import ru.mrcpp.appdrive.ui.login.ActivityLogin;
import ru.mrcpp.appdrive.ui.registration.ActivityRegistration;

public class ActivitySplash extends ActivityBase {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getActivityComponent().injectSplash(this);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.entrance)
    void driver() {
        startActivity(new Intent(this, ActivityLogin.class));
    }

    @OnClick(R.id.registration)
    void admin() {
        startActivity(new Intent(this, ActivityRegistration.class));
    }
}
