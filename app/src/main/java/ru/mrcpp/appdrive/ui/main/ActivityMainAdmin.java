package ru.mrcpp.appdrive.ui.main;

import android.os.Bundle;

import butterknife.ButterKnife;
import ru.mrcpp.appdrive.R;
import ru.mrcpp.appdrive.ui.base.ActivityBase;

public class ActivityMainAdmin extends ActivityBase {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_admin);
        getActivityComponent().injectMainAdmin(this);
        ButterKnife.bind(this);
    }
}
