package ru.mrcpp.appdrive;

public interface Constants {

    String NULL = "null";

    int MODE_ADMIN = 0;
    int MODE_DRIVER = 1;

    int FALSE = 0;
    int TRUE = 1;

    int STATUS_PENDING = 0;// ожидает подтверждения администратором
    int STATUS_REJECTED = 1;// отклонен администратором
    int STATUS_ONLINE = 2;// вне очереди
    int STATUS_IN_TUNE = 3;// в очереди

    String EXTRA_MESSAGE = "message";
}
/*
Описание:
1.	Регистрация водителя в системе. (Проводится на моб.приложении)
1.1.	Водитель запускает на своем моб.устройстве приложение. В случае если его IMEI не зарегистрирован в системе он должен ввести свой номер телефона, ФИО – в разных полях затем при нажатии кнопки «Зарегистрироваться» эти данные отправляются в базу вместе с IMEI.
1.2.	 Далее водителю отправляется SMS с кодом подтверждения регистрации на моб.телефон указанный при регистрации.
1.3.	Водитель в водит код подтверждения в моб.приложении и параметры (моб.телефон, iMEI, Фамилия, Имя, Отчество) записывается в базу в таблицу (Водители)
2.	Регистрация водителя в очереди
2.1.	 Для входа в систему водителю необходимо ввести (Номер авто, номер талона, позывной) и нажать на кнопку «войти», в момент попадания данной информации с базу, должна происходить привязка данного автомобиля к конкретному водителю.
2.2.	 После входа водителя в систему у него появляется кнопка №1 – «Встать в очередь». После нажатия на данную кнопку информация о желании данного водителя встать в очередь передается в базу. В системе у водителя отображается «статус»: Не подтвержден. (После входа водителя начинают отсылаться координаты в базу)
2.3.	 Далее информация передается в моб.приложение «Администратора». Администратору отображается след. Информация: КОМПАНИЯ КОТОРОЙ ПРИНАДЛЕЖИТ АВТО, Марка, Модель, Номер авто, ФИО водителя. При выборе определенного автомобиля в списке должна открыться форма в которой он может внести или отредактировать след.информацию: (курящий/некурящий и тд).  После внесения необходимой информации администратор может нажать кнопку «подтвердить» или «не подтвердить»
2.4.	После подтверждения Администратором водитель попадает в очередь и у него обновляется «статус»: в очереди.
3.	Заказ
3.1.	Информация о заказе уходит таксисту в моб.приложение и администратору вызова такси в его моб.приложения.
3.2.	Администратор вызова подтверждает посадку клиента в автомобиль к водителю. В момент подтверждения в системе (Базе) должно фиксироваться время посадки в таблице заказов.

Моб.приложение должно работать в след. режимах(Режим зависит от того кто его использует) :
1.	Водитель (в основном интерфейсе присутствует: Кнопка№1 – Встать в очередь, Покинуть очередь; Статус: Не подтвержден, В очереди; Таблица с информацией: в нее попадают информационные сообщения, Заказы.
2.	Администратор вызова. Присутствуют след.закладки: Очередь (в ней присутствуют водители которые не подтверждены в очереди); Посадка (в ней список заказов на подтверждение посадки); Таблица с информацией

Обязательные требования:
Необходимо обязательно передать исходные коды приложения, по итогам разработки приложения. Интерфейс значения не имеет, будем им заниматься сами. Для этого и нужны исходники.
*/