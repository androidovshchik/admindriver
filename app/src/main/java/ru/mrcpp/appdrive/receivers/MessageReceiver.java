package ru.mrcpp.appdrive.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import ru.mrcpp.appdrive.Constants;
import ru.mrcpp.appdrive.ui.main.ActivityMainDriver;

import timber.log.Timber;

public class MessageReceiver extends BroadcastReceiver {

    public static final String ACTION = "ru.mrcpp.appdrive.intent.action.MESSAGE";

    @Override
    public void onReceive(Context context, final Intent intent) {
        switch(intent.getAction()) {
            case ACTION:
                Timber.i(ACTION);
                if(ActivityMainDriver.getInstance() != null) {
                    ActivityMainDriver.getInstance()
                            .showMessage(intent.getIntExtra(Constants.EXTRA_MESSAGE, 0));
                }
                break;
            default:
                break;
        }
    }
}