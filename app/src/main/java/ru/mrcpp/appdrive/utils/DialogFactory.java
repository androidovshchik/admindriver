package ru.mrcpp.appdrive.utils;

import android.content.Context;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.LinearLayout;

public final class DialogFactory {

    public static AlertDialog.Builder createDefault(Context context, String title) {
        return new AlertDialog.Builder(context)
                .setTitle(title)
                .setPositiveButton(android.R.string.ok, null);
    }

    public static AlertDialog.Builder createDefault(Context context, @StringRes int titleResource) {
        return createDefault(context, context.getString(titleResource));
    }

    public static AppCompatEditText createInput(Context context, int type) {
        final AppCompatEditText code = new AppCompatEditText(context);
        code.setInputType(type);
        return code;
    }

    public static LinearLayout createLayout(Context context, View view) {
        final LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(ViewUtil.dp2px(20), ViewUtil.dp2px(20), ViewUtil.dp2px(20), 0);
        layout.addView(view, params);
        return layout;
    }
}
