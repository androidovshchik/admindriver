package ru.mrcpp.appdrive;

import android.app.Application;
import android.content.Context;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

import ru.mrcpp.appdrive.data.local.Prefs;
import ru.mrcpp.appdrive.injection.component.ApplicationComponent;
import ru.mrcpp.appdrive.injection.component.DaggerApplicationComponent;
import ru.mrcpp.appdrive.injection.module.ApplicationModule;

import timber.log.Timber;

public class AppDrive extends Application {

    private static AppDrive instance;

    ApplicationComponent applicationComponent;

    private static OkHttpClient client;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        Prefs.getInstance().printAll();
    }

    public static String currentActivity = Constants.NULL;

    public static void onActivityStart(String activityClass) {
        currentActivity = activityClass;
    }

    public static void onActivityStop() {
        currentActivity = Constants.NULL;
    }

    public synchronized static AppDrive getInstance() {
        return instance;
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    public static OkHttpClient getClient() {
        return client;
    }

    public ApplicationComponent getComponent() {
        if(applicationComponent == null) {
            applicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .build();
        }
        return applicationComponent;
    }

    public void setComponent(ApplicationComponent applicationComponent) {
        this.applicationComponent = applicationComponent;
    }
}
