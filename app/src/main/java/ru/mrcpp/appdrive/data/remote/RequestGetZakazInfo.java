package ru.mrcpp.appdrive.data.remote;

import org.json.JSONException;
import org.json.JSONObject;

import ru.mrcpp.appdrive.AppDrive;

import timber.log.Timber;

public class RequestGetZakazInfo extends RequestBase {

    public RequestGetZakazInfo(RequestListener listen) {
        listener = listen;
    }

    public void enqueue(String sessionId, long idZakaz) {
        JSONObject json = new JSONObject();
        try {
            json.put("session_id", sessionId);
            json.put("id_zakaz", idZakaz);
        } catch (JSONException e) {
            Timber.e(e.toString());
        }
        AppDrive.getClient().newCall(RequestUtil.getRequest(json, "get_zakazinfo")).enqueue(this);
    }
}
