package ru.mrcpp.appdrive.data.local;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Map;

import ru.mrcpp.appdrive.AppDrive;
import ru.mrcpp.appdrive.BuildConfig;

import timber.log.Timber;

public class Prefs {

    private static Prefs instance = null;

    public static Prefs getInstance() {
        if(instance == null) {
            instance = new Prefs(AppDrive.getContext());
        }
        return instance;
    }

    /* SharedPreferences parameters */
    public static final String STR_IMEI = "imei";
    public static final String STR_TOKEN = "token";
    public static final String STR_SMS = "sms";
    public static final String STR_SESSION_ID = "session_id";;
    public static final String STR_JSON_USER_INFO = "user_info";
    public static final String INT_MODE = "mode";
    public static final String LONG_TOKEN_TIME_RECEIVING = "token_time_receiving";

    /* Util strings */
    public static final String EMPTY = "";

    private SharedPreferences pref;

    public Prefs(Context context) {
        pref = PreferenceManager.getDefaultSharedPreferences(context);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // GET functions

    /* Strings */

    @SuppressWarnings("unused")
    public String getStr(String name) {
        return pref.getString(name, EMPTY).trim();
    }

    @SuppressWarnings("unused")
    public <T> String getStr(String name, T def) {
        return pref.getString(name, str(def)).trim();
    }

    /* Booleans */

    @SuppressWarnings("unused")
    public boolean getBool(String name) {
        return pref.getBoolean(name, false);
    }

    @SuppressWarnings("unused")
    public boolean getBool(String name, boolean def) {
        return pref.getBoolean(name, def);
    }

    /* Integers */

    @SuppressWarnings("unused")
    public int getInt(String name) {
        return pref.getInt(name, 0);
    }

    @SuppressWarnings("unused")
    public int getInt(String name, int def) {
        return pref.getInt(name, def);
    }

    /* Longs */

    @SuppressWarnings("unused")
    public long getLong(String name) {
        return pref.getLong(name, 0);
    }

    @SuppressWarnings("unused")
    public long getLong(String name, long def) {
        return pref.getLong(name, def);
    }

    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // PUT functions

    /* Strings */

    @SuppressWarnings("unused")
    public <T> void putStr(String name, T value) {
        pref.edit().putString(name, str(value)).apply();
    }

    /* Booleans */

    @SuppressWarnings("unused")
    public void putBool(String name, boolean value) {
        pref.edit().putBoolean(name, value).apply();
    }

    /* Integers */

    @SuppressWarnings("unused")
    public void putInt(String name, int value) {
        pref.edit().putInt(name, value).apply();
    }

    /* Longs */

    @SuppressWarnings("unused")
    public void putLong(String name, long value) {
        pref.edit().putLong(name, value).apply();
    }

    /* Reset */

    @SuppressWarnings("unused")
    public void resetBool(String name) {
        pref.edit().putBoolean(name, false).apply();
    }

    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /* Controls functions */

    @SuppressWarnings("unused")
    public boolean has(String name) {
        return pref.contains(name);
    }

    @SuppressWarnings("unused")
    public void clear() {
        pref.edit().clear().apply();
    }

    @SuppressWarnings("unused")
    public void remove(String name) {
        if (has(name)) {
            pref.edit().remove(name).apply();
        }
    }

    /* Utils functions */

    @SuppressWarnings("unused")
    public <T> String str(T value) {
        return String.class.isInstance(value)? ((String) value).trim() : String.valueOf(value);
    }

    @SuppressWarnings("unused")
    public void printAll() {
        if (BuildConfig.DEBUG) {
            Map<String, ?> keys = pref.getAll();
            if (keys == null) {
                return;
            }
            Timber.i("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            Timber.i("Printing all sharedPreferences");
            for (Map.Entry<String,?> entry : keys.entrySet()) {
                String log = entry.getKey() + ": " + entry.getValue();
                Timber.i(log);
            }
            Timber.i("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        }
    }
}
