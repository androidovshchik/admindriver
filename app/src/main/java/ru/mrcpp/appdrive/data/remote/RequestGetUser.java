package ru.mrcpp.appdrive.data.remote;

import org.json.JSONException;
import org.json.JSONObject;

import ru.mrcpp.appdrive.AppDrive;

import timber.log.Timber;

public class RequestGetUser extends RequestBase {

    public RequestGetUser(RequestListener listen) {
        listener = listen;
    }

    public void enqueue(String sessionId) {
        JSONObject json = new JSONObject();
        try {
            json.put("session_id", sessionId);
        } catch (JSONException e) {
            Timber.e(e.toString());
        }
        AppDrive.getClient().newCall(RequestUtil.getRequest(json, "get_user")).enqueue(this);
    }
}
