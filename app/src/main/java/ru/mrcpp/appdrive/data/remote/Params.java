package ru.mrcpp.appdrive.data.remote;

import okhttp3.MediaType;

public interface Params {

    String DEBUG_URL = "http://msk.rusoil.net/app/serv.php?function=";

    MediaType JSON = MediaType.parse("application/json; charset=utf-8");
}
