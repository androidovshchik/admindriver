package ru.mrcpp.appdrive.data.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Driver implements Parcelable {

    public long id;
    public long filter;
    public String name;
    public String image;
    public int count;

    public Driver()  {
        super();
    }

    public Driver(Parcel in) {
        super();
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeLong(this.filter);
        dest.writeString(this.name);
        dest.writeString(this.image);
        dest.writeInt(this.count);
    }

    public void readFromParcel(Parcel in) {
        this.id = in.readLong();
        this.filter = in.readLong();
        this.name = in.readString();
        this.image = in.readString();
        this.count = in.readInt();
    }

    public static final Creator<Driver> CREATOR = new Creator<Driver>() {

        public Driver createFromParcel(Parcel source) {
            return new Driver(source);
        }

        public Driver[] newArray(int size) {
            return new Driver[size];
        }
    };
}
