package ru.mrcpp.appdrive.data.remote;

import android.support.annotation.StringRes;

import org.json.JSONObject;

public interface RequestListener {

    void onSuccess(JSONObject response);

    void onError(String message, @StringRes int resString);
}
