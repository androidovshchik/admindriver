package ru.mrcpp.appdrive.data.remote;

import org.json.JSONException;
import org.json.JSONObject;

import ru.mrcpp.appdrive.AppDrive;

import timber.log.Timber;

public class RequestSubscribe extends RequestBase {

    public RequestSubscribe(RequestListener listen) {
        listener = listen;
    }

    public void enqueue(String sessionId, String deviceId, String deviceType,
                        String deviceToken) {
        JSONObject json = new JSONObject();
        try {
            json.put("session_id", sessionId);
            json.put("device_id", deviceId);
            json.put("device_type", deviceType);
            json.put("device_token", deviceToken);
        } catch (JSONException e) {
            Timber.e(e.toString());
        }
        AppDrive.getClient().newCall(RequestUtil.getRequest(json, "subscribe")).enqueue(this);
    }
}
