package ru.mrcpp.appdrive.data.remote;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Locale;

import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import timber.log.Timber;

public class RequestUtil {

    public static String getResponse(Response response) throws IOException {
        if (!response.isSuccessful()) {
            return null;
        }
        String res = response.body().string();
        Timber.i(res);
        return res;
    }

    public static Request getRequest(JSONObject json, String function) {
        RequestBody body = RequestBody.create(Params.JSON, json.toString());
        return new Request.Builder()
                .url(String.format(Locale.getDefault(), "%s%s", Params.DEBUG_URL, function))
                .post(body)
                .build();
    }
}
