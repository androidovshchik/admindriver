package ru.mrcpp.appdrive.data.remote;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

import ru.mrcpp.appdrive.R;

import timber.log.Timber;

public class RequestBase implements Callback {

    protected RequestListener listener;

    @SuppressWarnings("unused")
    public void setRequestListener(RequestListener listen) {
        listener = listen;
    }

    public RequestBase() {}

    @Override
    public void onFailure(Call call, IOException e) {
        Timber.e(e.toString());
        if (listener != null) {
            listener.onError(null, R.string.no_connection);
        }
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {
        String res = RequestUtil.getResponse(response);
        if (listener != null) {
            if (res == null || res.isEmpty()) {
                listener.onError(null, R.string.error_request);
                return;
            }
            try {
                JSONObject object = new JSONObject(res);
                if (object.has("error")) {
                    JSONObject error = object.getJSONObject("error");
                    listener.onError(error.getString("comment"), 0);
                } else {
                    listener.onSuccess(object);
                }
            } catch (JSONException e) {
                Timber.e(e.toString());
                listener.onError(null, R.string.error_json);
            }
        }
    }
}
