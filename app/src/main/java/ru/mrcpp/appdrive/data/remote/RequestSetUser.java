package ru.mrcpp.appdrive.data.remote;

import org.json.JSONException;
import org.json.JSONObject;

import ru.mrcpp.appdrive.AppDrive;

import timber.log.Timber;

public class RequestSetUser extends RequestBase {

    public RequestSetUser(RequestListener listen) {
        listener = listen;
    }

    public void enqueue(String sessionId, String userInfo) {
        JSONObject json = new JSONObject();
        try {
            json.put("session_id", sessionId);
            json.put("user_info", userInfo);
        } catch (JSONException e) {
            Timber.e(e.toString());
        }
        AppDrive.getClient().newCall(RequestUtil.getRequest(json, "set_user")).enqueue(this);
    }
}
