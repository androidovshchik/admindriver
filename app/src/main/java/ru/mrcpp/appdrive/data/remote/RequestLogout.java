package ru.mrcpp.appdrive.data.remote;

import org.json.JSONException;
import org.json.JSONObject;

import ru.mrcpp.appdrive.AppDrive;

import timber.log.Timber;

public class RequestLogout extends RequestBase {

    public RequestLogout(RequestListener listen) {
        listener = listen;
    }

    public void enqueue(String sessionId, String deviceId) {
        JSONObject json = new JSONObject();
        try {
            json.put("session_id", sessionId);
            json.put("device_id", deviceId);
        } catch (JSONException e) {
            Timber.e(e.toString());
        }
        AppDrive.getClient().newCall(RequestUtil.getRequest(json, "logout")).enqueue(this);
    }
}
