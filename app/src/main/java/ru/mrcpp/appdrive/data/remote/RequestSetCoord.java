package ru.mrcpp.appdrive.data.remote;

import org.json.JSONException;
import org.json.JSONObject;

import ru.mrcpp.appdrive.AppDrive;

import timber.log.Timber;

public class RequestSetCoord extends RequestBase {

    public RequestSetCoord(RequestListener listen) {
        listener = listen;
    }

    public void enqueue(String sessionId, long latitude, long longitude) {
        JSONObject json = new JSONObject();
        try {
            json.put("session_id", sessionId);
            json.put("latitude", latitude);
            json.put("longitude", longitude);
        } catch (JSONException e) {
            Timber.e(e.toString());
        }
        AppDrive.getClient().newCall(RequestUtil.getRequest(json, "set_coord")).enqueue(this);
    }

}
