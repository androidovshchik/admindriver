package ru.mrcpp.appdrive.data.local;

import android.content.ContentValues;
import android.database.Cursor;

import ru.mrcpp.appdrive.Constants;
import ru.mrcpp.appdrive.data.model.Driver;

public class Db {

    public static final String DATABASE_NAME = "app_drive.sqlite";
    public static final int DATABASE_VERSION = 1;

    public static final String TABLE_DRIVERS = "drivers";

    public static final String COLUMN_ID = "id";

    public abstract static class MyMessagesTable {

        public static final String COLUMN_CATEGORY = "category";

        public static ContentValues toContentValues(Driver driver) {
            ContentValues values = new ContentValues();
            /*values.put(COLUMN_CATEGORY, myMessage.category);
            values.put(COLUMN_AUTHOR, myMessage.author);
            values.put(COLUMN_MESSAGE, myMessage.message);
            values.put(COLUMN_IS_POSTED, myMessage.isPosted? Constants.TRUE : Constants.FALSE);*/
            return values;
        }

        public static Driver parseCursor(Cursor cursor) {
            Driver driver = new Driver();
            /*myMessage.id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID));
            myMessage.category = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_CATEGORY));
            myMessage.author = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_AUTHOR));
            myMessage.message = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_MESSAGE));
            myMessage.isPosted = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_IS_POSTED)) == Constants.TRUE;*/
            return driver;
        }
    }
}
