package ru.mrcpp.appdrive.data.remote;

import org.json.JSONException;
import org.json.JSONObject;

import ru.mrcpp.appdrive.AppDrive;

import timber.log.Timber;

public class RequestConfirm extends RequestBase {

    public RequestConfirm(RequestListener listen) {
        listener = listen;
    }

    public void enqueue(String phone, String imei, String token, String code) {
        JSONObject json = new JSONObject();
        try {
            json.put("phone", phone);
            json.put("imei", imei);
            json.put("token", token);
            json.put("code", code);
        } catch (JSONException e) {
            Timber.e(e.toString());
        }
        AppDrive.getClient().newCall(RequestUtil.getRequest(json, "confirm")).enqueue(this);
    }
}