package ru.mrcpp.appdrive.data.remote;

import org.json.JSONException;
import org.json.JSONObject;

import ru.mrcpp.appdrive.AppDrive;

import timber.log.Timber;

public class RequestSetPhoto extends RequestBase {

    public RequestSetPhoto(RequestListener listen) {
        listener = listen;
    }

    public void enqueue(String sessionId, String photo) {
        JSONObject json = new JSONObject();
        try {
            json.put("session_id", sessionId);
            json.put("photo", photo);
        } catch (JSONException e) {
            Timber.e(e.toString());
        }
        AppDrive.getClient().newCall(RequestUtil.getRequest(json, "set_photo")).enqueue(this);
    }

}
