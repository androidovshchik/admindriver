package ru.mrcpp.appdrive.data.remote;

import org.json.JSONException;
import org.json.JSONObject;

import ru.mrcpp.appdrive.AppDrive;

import timber.log.Timber;

public class RequestRegister extends RequestBase {

    public RequestRegister(RequestListener listen) {
        listener = listen;
    }

    public void enqueue(String phone, String imei) {
        JSONObject json = new JSONObject();
        try {
            json.put("phone", phone);
            json.put("imei", imei);
        } catch (JSONException e) {
            Timber.e(e.toString());
        }
        AppDrive.getClient().newCall(RequestUtil.getRequest(json, "register")).enqueue(this);
    }
}
