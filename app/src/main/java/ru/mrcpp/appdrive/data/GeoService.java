package ru.mrcpp.appdrive.data;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONObject;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import ru.mrcpp.appdrive.Constants;
import ru.mrcpp.appdrive.R;
import ru.mrcpp.appdrive.data.remote.RequestListener;
import ru.mrcpp.appdrive.data.remote.RequestSetCoord;
import ru.mrcpp.appdrive.receivers.MessageReceiver;
import ru.mrcpp.appdrive.utils.ComponentUtil;
import ru.mrcpp.appdrive.utils.NetworkUtil;

import timber.log.Timber;

public class GeoService extends Service implements LocationListener, RequestListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private RequestSetCoord request;

    private ScheduledFuture<?> sender;

    private GoogleApiClient googleApiClient;

    private long latitude;
    private long longitude;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, GeoService.class);
    }

    public static boolean isRunning(Context context) {
        return ComponentUtil.isServiceRunning(context, GeoService.class);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        buildGoogleApiClient();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, final int startId) {
        if(!NetworkUtil.isNetworkConnected(this) || !checkPermissions()) {
            stopSelf(startId);
            return START_NOT_STICKY;
        }
        request = new RequestSetCoord(this);
        googleApiClient.connect();
        ScheduledExecutorService timer = Executors.newScheduledThreadPool(1);
        sender = timer.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                Timber.i("Checking driver status...");
                request.enqueue("88e1a82fhh5iii680bm7vafge0", latitude, longitude);
            }
        }, 0, 5, TimeUnit.SECONDS);
        return START_STICKY;
    }

    private boolean checkPermissions() {
        return checkAccessFineLocation();// && checkAccessWifiStatePermission()
                //&& checkReadPhoneStatePermission();
    }

    private boolean checkAccessFineLocation() {
        return checkCallingOrSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }

    synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        if(checkCallingOrSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(1000);
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient,
                    locationRequest, this);
            Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (lastLocation != null) {
                latitude = Math.round(lastLocation.getLatitude());
                longitude = Math.round(lastLocation.getLongitude());
                Timber.e("onConnected");
                Timber.e("latitude: %d", latitude);
                Timber.e("longitude: %d", longitude);
                //syncWeather(latitude, longitude);
            } else {
                Timber.e("onConnected FAILED");
                Intent sync = new Intent(MessageReceiver.ACTION);
                sync.putExtra(Constants.EXTRA_MESSAGE, R.string.error_geo_data);
                sendBroadcast(sync);
            }
        }
    }

    @Override
    public void onSuccess(JSONObject response) {

    }

    @Override
    public void onError(final String message, @StringRes final int resString) {
        //showMessage(message, resString);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Timber.e("onConnectionSuspended: %d", i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Timber.e("onConnectionFailed: %s", connectionResult.getErrorMessage());
        buildGoogleApiClient();
    }

    @Override
    public void onLocationChanged(Location location) {
        Timber.e("onLocationChanged");
        if (location == null) {
            Timber.e("location == null");
            return;
        }
        latitude = Math.round(location.getLatitude());
        longitude = Math.round(location.getLongitude());
        Timber.e("latitude: %d", latitude);
        Timber.e("longitude: %d", longitude);
        //syncWeather(latitude, longitude);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (sender != null && !sender.isCancelled()) {
            sender.cancel(false);
        }
        googleApiClient.disconnect();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}